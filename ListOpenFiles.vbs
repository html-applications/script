' ListOpenFiles.vbs
' Version: 1.0.0
' Description: List files opened on fileserver
' --------------------------------------------
If Not InStr(LCase(WScript.FullName),"cscript.exe") > 1 Then
	Set oArgs = WScript.Arguments
	If oArgs.Count = 0 Then
		comp = ""
		title = "List Open Files"
	Else
		comp = oArgs(0)
		title = "List Open Files on \\" & comp
	End If
	CreateObject("WScript.Shell").Run "cmd /f:on /t:1b /k title " & title & " & mode con:cols=180 & cscript /nologo """ & WScript.ScriptFullName & """ " & comp
	WScript.Quit
End If
' --------------------------------------------

Set oArgs = WScript.Arguments
If oArgs.Count = 0 Then
	strComputer = InputBox("Enter computer name:", "List Open Files", ".")
	If Len(strComputer) = 0 Then WScript.Quit
Else
	strComputer = oArgs(0)
End If

Set oFileService = GetObject("WinNT://" & strComputer & "/LanmanServer")
On Error Resume Next
For Each oResource In oFileService.Resources
	WScript.Echo oResource.User & vbTab &  oResource.Path
Next
