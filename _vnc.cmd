@ECHO off
REM Full version with utils <https://bitbucket.org/html-applications/lan-admin/downloads/cm_vnc.zip>
TITLE UltraVNC
COLOR 71
CD /d "%~dp0"
ECHO      ====================================
ECHO          Connect with UltraVNC v.1.4.0
ECHO      ====================================
ECHO    Usage: _vnc [computer] [login] [password]
ECHO.
SET comp=%1
IF [%comp%]==[] SET /p comp=Enter computer name for connect:
IF [%comp%]==[] GOTO :error

TITLE UltraVNC : %comp%

IF not exist \\%comp%\C$\ (
	IF [%2]==[] (
		NET use * \\%comp%\c$ /savecred
	) ELSE (
		NET use * \\%comp%\c$ %3 /user:%2
	)
	IF errorlevel 1 GOTO :error
	FOR /F "tokens=2" %%i IN ('NET use^|FIND /i "\\%comp%\c$"') DO NET use %%i /delete
)

SET install_dir=windows\uvnc

IF not exist \\%comp%\C$\%install_dir%\winvnc.exe (
	MD \\%comp%\C$\%install_dir%
	<nul SET /p a=Copy to computer %comp% files UltraVNC
	IF exist "\\%comp%\C$\Windows\SysWOW64" (
		ECHO 64...
		XCOPY x64\winvnc.exe \\%comp%\C$\%install_dir%\ /f /c /y
	) ELSE (
		ECHO 32...
		XCOPY winvnc.exe \\%comp%\C$\%install_dir%\ /f /c /y
	)
	IF errorlevel 1 GOTO :error
	XCOPY ultravnc.ini \\%comp%\C$\%install_dir%\ /f /c /y
	IF errorlevel 1 GOTO :error
)

SC \\%comp% query uvnc_service | FIND "uvnc_service"
IF errorlevel 1 (
	ECHO.
	ECHO Create service...
	SC \\%comp% create uvnc_service binpath= "c:\%install_dir%\winvnc.exe -service" start= auto
	IF errorlevel 1 GOTO :error
)

SC \\%comp% query uvnc_service | FIND "RUNNING"
IF errorlevel 1 (
	ECHO.
	ECHO Start service...
	SC \\%comp% start uvnc_service
	IF errorlevel 1 GOTO :error
)
ECHO.
<nul SET /p a=Wait for opening of the port connection
:tcping
<nul SET /p a=.
TcPing -n 1 %comp% 5900 >nul 2<&1
if errorlevel 1 goto :tcping
SET errorlevel=0
ECHO.

ECHO.
<nul SET /p a=Run VNCViewer
IF exist %windir%\SysWOW64 (
	ECHO 64...
	ECHO                                    = DON'T CLOSE THIS CONSOLE! =
	x64\vncviewer.exe %comp% -password gfhjkm
) else (
	ECHO 32...
	ECHO                                    = DON'T CLOSE THIS CONSOLE! =
	vncviewer.exe %comp% -password gfhjkm
)
IF errorlevel 1 GOTO :error

ECHO.
ECHO Stop and delete service...
SC \\%comp% stop uvnc_service
IF errorlevel 1 GOTO :error
SC \\%comp% delete uvnc_service
IF errorlevel 1 GOTO :error

ECHO.
ECHO Wait for completion of the process winvnc.exe...
PING 127.0.0.1 -n 9 > nul

ECHO.
<nul SET /p a=Remove UltraVNC files
:rd
echo.|taskkill /s %comp% /im winvnc.exe >nul 2<&1
RD /s /q \\%comp%\C$\%install_dir% 2>nul
<nul SET /p a=.
IF exist \\%comp%\C$\%install_dir% GOTO :rd
ECHO.
ECHO Bye!
EXIT /b

:error
PAUSE
EXIT /b 1
