'  File:         List2String.vbs
'  Description:  Скрипт для загрузки выделенных в Total Commander файлов в заданную программу
'  Version:      1.2.1
'  Author:       mozers™
'  Comments:     Программа должна поддерживать загрузку нескольких файлов с ком.строки.
'                Т.е. обрабатывать команду вида:
'                programm.exe [/param1 /param2] file1.ext file2.ext file3.ext
'  Sample:       Создаем кнопку на панели инструментов (или команду в меню "Запуск"):
'                Команда: %COMMANDER_PATH%\Utils\WSH\List2String.vbs "%ProgramFiles%\Winamp\Winamp.exe" /ADD
'                Параметры: %L
' _________________________________________________________

Option Explicit
Dim WshShell, FSO, sProg, objArgs, FileList, sOneFile, sStringFiles, count, param, i
Set objArgs = WScript.Arguments
Set WshShell = WScript.CreateObject("WScript.Shell")
sProg = WshShell.ExpandEnvironmentStrings(objArgs(0))
count = objArgs.Count
If count < 2 Then
	WshShell.Run """" & sProg & """", 1, False
Else
	param = ""
	For i = 1 To count-2
		param = param + " " + objArgs(i)
	Next
	Set FSO = CreateObject("Scripting.FileSystemObject")
	Set FileList = FSO.OpenTextFile(objArgs(count-1), 1, False)
	Do While Not FileList.AtEndOfStream
		sOneFile = FileList.ReadLine
		If Right(sOneFile, 1) = "\" Then sOneFile = Left(sOneFile, Len(sOneFile) - 1) 'Нужно для немногих программ (например для SCDWriter)
		sStringFiles = sStringFiles & " """ & sOneFile & """"
	Loop
	FileList.Close
	WshShell.Run """" & sProg & """ " & param & " " & sStringFiles, 1, False
End If
WScript.Quit
