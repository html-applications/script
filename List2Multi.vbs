'  File:         List2Multi.vbs
'  Description:  Скрипт для загрузки выделенных в Total Commander файлов в заданную программу
'  Version:      2.0.2
'  Author:       mozers™, Dystopian
'  Comments:     Предназначен для программ НЕ поддерживающих загрузку нескольких файлов с ком.строки.
'                Поэтому скрипт вызывает программу многократно:
'                programm.exe [/param1 /param2] file1.ext
'                programm.exe [/param1 /param2] file2.ext
'                programm.exe [/param1 /param2] file3.ext
'  Sample:       Создаем кнопку на панели инструментов (или команду в меню "Запуск"):
'                Команда: %COMMANDER_PATH%\Utils\WSH\List2Multi.vbs "%ProgramFiles%\WinRAR\WinRAR.exe" X %W
'                Параметры: %L
'
'                В поле "Команда:" можно включить любое количество параметров вызываемой программы, а так же параметры
'                %F - если присутствует, то имя файла, обрабатываемого программой, будет вставлятся не в конец командной строки
'                     (после параметров программы) а в заданное этим параметром место.
'                     Предназначено для программ, в которых параметы необходимо задавать после имени файла;
'                %W - должно указываться в конце командной строки (предназначено для последовательного запуска программы).
'                     Без этого параметра все экземпляры заданной программы стартуют практически одновременно.
'                В поле "Параметры:" можно вписать только %L
' _________________________________________________________
Option Explicit
Dim WshShell, FSO, oArgs, sCommand, sParams, sParam, i, iCount, sFileList, oFileList, sFileName, bWait

Set WshShell = CreateObject("WScript.Shell")
Set FSO = CreateObject("Scripting.FileSystemObject")

Set oArgs = WScript.Arguments
iCount = oArgs.Count

sCommand = """" & WshShell.ExpandEnvironmentStrings(oArgs(0)) & """"
sParams = ""
If iCount > 2 Then
	For i = 1 To iCount - 2
		sParam = oArgs(i)
		If (i = iCount - 2) And (sParam = "%W") Then
			bWait = True
		Else
			If InStr(sParam, " ") Then sParam = """" & sParam & """"
			sParams = sParams & " " & sParam
		End If
	Next
End If
If sParams <> "" Then
	If InStr(sParams, "%F") Then
		sParams = Replace(sParams, "%F", "%FileName%")
		sCommand = sCommand & " " & sParams
	Else
		sCommand = sCommand & " " & sParams & " %FileName%"
	End If
End If

sFileList = oArgs(iCount - 1)
If FSO.FileExists(sFileList) Then
	Set oFileList = FSO.OpenTextFile(sFileList)
	Do While Not oFileList.AtEndOfStream
		sFileName = """" & oFileList.ReadLine & """"
		WshShell.Run Replace(sCommand, "%FileName%", sFileName), 1, bWait
	Loop
	oFileList.Close
End If
